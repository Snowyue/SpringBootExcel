package com.yue.controller;


import com.yue.commom.ExcelImportSheet;
import com.yue.entity.Student;
import com.yue.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;


import org.apache.poi.ss.usermodel.*;

import java.io.*;
import java.util.*;



/**
 * <p>
 * 管理员 前端控制器
 * </p>
 *
 * @author jobob
 * @since 2023-12-06
 */
@RestController
@RequestMapping("/yue/")
public class StudentController {



    @Autowired
    private IStudentService studentService;

    @GetMapping("/index")
    public void adminIndex() {
        Map<String , String> map = new HashMap<>();
        //表头与键值对的映射关系
        map.put("学号", "id");
        map.put("姓名" , "name");
        map.put("科目" , "subject");
        map.put("分数" , "grade");
        map.put("班级" , "className");
        map.put("任课教师" , "teacher");
        map.put("是否缺课" , "ifCutClass");
        try(
                //这里面的对象会自动关闭
                InputStream in = new FileInputStream(new File("C:\\Test\\Student.xlsx"));
                //用流来构建工作簿对象
                Workbook workbook = ExcelImportSheet.getTypeFromExtends(in , "Student.xlsx")
        ) {

            //根据名称获取单张表对象 也可以使用getSheetAt(int index)获取单张表的对象 获取第一张表
            Sheet sheet = workbook.getSheetAt(0);
            List<Student> list = ExcelImportSheet.getListFromExcel(sheet , Student.class , map);
            studentService.saveBatch(list);
            for (Student student : list) {

                //底层数据库操作 insert什么的
                if(student.getName() == null || "".equals(student.getName())
                        ||student.getSubject()== null || "".equals(student.getSubject()) ){
                    System.out.println("222222222");
                }
                System.out.println(student.toString());
            }
        }catch(IOException exception) {
            exception.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            //写着好看的
        }
    }

}


