package com.yue.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 管理员
 * </p>
 *
 * @author jobob
 * @since 2023-12-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 管理员id
     */
    private String id;

    /**
     * 管理员名称
     */
    private String name;

    /**
     * 管理员密码
     */
    private String subject;

    /**
     * 管理员密码
     */
    @TableField("className")
    private String className;

    /**
     * 管理员密码
     */
    private String grade;

    /**
     * 管理员密码
     */
    @TableField("ifCutClass")
    private Integer ifCutClass;

    /**
     * 管理员密码
     */
    private String teacher;


}
