package com.yue.mapper;

import com.yue.entity.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 管理员 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2023-12-06
 */
public interface StudentMapper extends BaseMapper<Student> {

}
