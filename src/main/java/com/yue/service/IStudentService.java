package com.yue.service;

import com.yue.entity.Student;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 管理员 服务类
 * </p>
 *
 * @author jobob
 * @since 2023-12-06
 */
public interface IStudentService extends IService<Student> {


}
